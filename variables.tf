variable region {
  default = "us-east-1"
}

variable "prefix" {
  default = "demo"
}

variable "name" {
  default = "demo"
}

variable "package" {
  default = "data/docker.zip"
}
