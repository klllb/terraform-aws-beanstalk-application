# Elastic Beanstalk

resource "aws_elastic_beanstalk_application" "default" {
  name = "${var.name}"
}

resource "aws_s3_bucket" "default" {
  bucket = "${var.prefix}-beanstalk-${aws_elastic_beanstalk_application.default.name}-version"
}

resource "aws_s3_bucket_object" "default" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "${var.package}"
  source = "${var.package}"
}

resource "aws_elastic_beanstalk_application_version" "default" {
  name = "LatestVersion"
  application = "${aws_elastic_beanstalk_application.default.name}"
  bucket = "${aws_s3_bucket.default.id}"
  key = "${aws_s3_bucket_object.default.id}"
}
