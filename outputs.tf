output "name" {
  value = "${aws_elastic_beanstalk_application.default.name}"
}

output "bucket_name" {
  value = "${aws_s3_bucket.default.id}"
}

output "version_label" {
  value = "${aws_elastic_beanstalk_application_version.default.name}"
}
